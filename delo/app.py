#!coding=utf-8
from flask import Flask
app = Flask(__name__, static_folder='static')

app.config.from_object('delo.defaults')
app.config.from_envvar('CONF') #  , silent=True)

from .frontend import frontend

app.register_blueprint(frontend)


import decimal


@app.template_filter('money')
def money(s):
    if isinstance(s, decimal.Decimal):
        s = s.quantize(decimal.Decimal(1))

    return u'{0} грн'.format(s)


@app.template_filter('money_nocur')
def money_nocur(s):
    if isinstance(s, decimal.Decimal):
        s = s.quantize(decimal.Decimal(1))

    return str(s)


@app.template_filter('distance')
def money(s):
    if s > 1000:
        return u'%d км' % (s/1000)
    return u'%d м' % s


import re


@app.template_filter('latlng')
def latlng(s):
    m = re.match('POINT\((\d+\.\d+) (\d+\.\d+)\)', str(s))
    if not m:
        return None

    return ','.join([m.group(1), m.group(2)])

