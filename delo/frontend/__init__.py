import decimal
from flask import Blueprint, render_template, request, redirect, url_for, current_app, jsonify
from delo.db import *
from delo.liqpay import Liqpay, STATUS_SUCCESS, STATUS_FAILURE, STATUS_WAIT_SECURE
from sqlalchemy import func

frontend = Blueprint('frontend', __name__, template_folder='templates')


@frontend.route('/api/dela/<lat>|<lng>.json')
def dela_api(lat, lng):
    loc = "POINT({0} {1})".format(lat, lng)

    distance = func.st_distance(loc, Delo.position)

    data = []

    for delo, distance in db.session.query(Delo, distance).filter_by(is_approved=True).order_by(distance):
        print delo, distance
        data.append(dict(
            id=delo.id,
            title=delo.title,
            teaser=delo.teaser,
            num_backers=delo.num_backers,
            goal=int(delo.goal),
            pledged=int(delo.pledged),
            distance=distance,
            image=''
        ))
    return jsonify(lat=lat, lng=lng, dela=data)


@frontend.route('/')
def index():
    print Delo.query.filter_by(is_approved=True)
    dela = Delo.query.filter_by(is_approved=True)[:3]
    return render_template('index.html', dela=dela)


@frontend.route('/discover/')
def discover():
    dela = Delo.query.filter_by(is_approved=True).all()
    return render_template('discover.html', dela=dela)


@frontend.route('/discover/near/')
def discover_near():
    pos = request.args['pos']
    name = request.args['name']
    if not pos:
        return redirect(url_for('frontend.discover'))

    lat, lng = [x.strip() for x in pos.split(' ')]

    loc = "POINT({0} {1})".format(lat, lng)

    distance = func.st_distance(loc, Delo.position)

    dela = list(db.session.query(Delo, distance).filter_by(is_approved=True).order_by(distance).all())

    return render_template('discover_near.html', dela=dela, pos=pos, name=name, lat=lat, lng=lng)


@frontend.route('/add/', methods=['GET', 'POST'])
def add():
    from .forms import AddDeloForm
    form = AddDeloForm()
    if form.validate_on_submit():
        d = Delo(curator_id=1,
                 title=form.title.data,
                 teaser=form.teaser.data,
                 description=DeloDescription(html=form.description.data),
                 goal=form.goal.data,
                 position="POINT({0} {1})".format(form.position_lat.data, form.position_lng.data),
                 approved_at=datetime.datetime.now()
                 )
        db.session.add(d)
        db.session.commit()
        return redirect(url_for('frontend.delo', delo_id=d.id))
    return render_template('delo_add.html', form=form)


@frontend.route('/add/done/')
def add_done():
    return render_template('delo_add_done.html')


@frontend.route('/about/')
def about():
    return render_template('about.html')


@frontend.route('/delo/<delo_id>/')
def delo(delo_id):
    delo = Delo.query.get_or_404(delo_id)

    is_backer = Pledge.query.filter_by(delo=delo, user_id=1).count() > 0

    return render_template('delo.html', delo=delo, is_backer=is_backer)


@frontend.route('/delo/<delo_id>/print/')
def delo_print(delo_id):
    delo = Delo.query.get_or_404(delo_id)

    return render_template('delo_print.html', delo=delo)


@frontend.route('/delo/<delo_id>/pledge/', methods=['GET', 'POST'])
def pledge(delo_id):
    delo = Delo.query.get_or_404(delo_id)

    # FIXME: check delo status

    if request.method == 'POST':
        p = Pledge(delo=delo, user_id=1, amount=decimal.Decimal(request.form['amount']))
        db.session.add(p)
        db.session.commit()
        return redirect(url_for('frontend.pledge_confirm', delo_id=delo.id, pledge_id=p.id))

    return render_template('pledge.html', delo=delo)


@frontend.route('/delo/<delo_id>/pledge/<pledge_id>/confirm/')
def pledge_confirm(delo_id, pledge_id):
    delo = Delo.query.get_or_404(delo_id)
    pledge = Pledge.query.get_or_404(pledge_id)

    lp = _get_liqpay()

    merchant_xml = lp.build_merchant_xml(
        order_id=pledge.id,
        amount=pledge.amount,
        currency='UAH',
        description=delo.title,
        result_url=url_for('frontend.pledge_done', delo_id=delo.id, pledge_id=pledge.id, _external=True),
        server_url=url_for('frontend.liqpay_ipn', _external=True),
    )

    print merchant_xml.xml
    return render_template('pledge_confirm.html', delo=delo, pledge=pledge, operation_xml=merchant_xml.encoded_xml, signature=merchant_xml.encoded_signature)


@frontend.route('/delo/<delo_id>/pledge/<pledge_id>/done/', methods=['GET', 'POST'])
def pledge_done(delo_id, pledge_id):
    delo = Delo.query.get_or_404(delo_id)
    pledge = Pledge.query.get_or_404(pledge_id)

    if request.method == 'POST':
        _handle_ipn()
        return redirect(url_for('frontend.pledge_done', delo_id=delo.id, pledge_id=pledge_id))

    return render_template('pledge_done.html',
                           delo=delo,
                           pledge=pledge,
                           is_success=pledge.last_status == STATUS_SUCCESS,
                           is_failure=pledge.last_status == STATUS_FAILURE,
                           is_wait=pledge.last_status == STATUS_WAIT_SECURE)


@frontend.route('/liqpay_ipn/')
def liqpay_ipn():
    _handle_ipn()


def _handle_ipn():
    import logging

    lp = _get_liqpay()

    opxml = request.form.get('operation_xml')
    signature = request.form.get('signature')
    response = lp.parse_merchant_response_xml(opxml, signature)

    pledge = Pledge.query.with_lockmode('update').get(response.order_id)

    logging.debug('LiqPay response status: %s' % response.status)

    pledge.last_status = response.status
    pledge.last_code = response.code
    pledge.last_description = response.description

    if response.status == STATUS_SUCCESS:
        Delo.query.filter_by(id=pledge.delo.id).update({Delo.pledged: Delo.pledged + pledge.amount, Delo.num_backers: Delo.num_backers + 1})
        pledge.completed_at = datetime.datetime.now()
    elif response.status == STATUS_FAILURE:
        pass
    elif response.status == STATUS_WAIT_SECURE:
        pass
    db.session.commit()


def _get_liqpay():
    print current_app.config['LIQPAY_MERCHANT'], current_app.config['LIQPAY_SECRET']
    return Liqpay(current_app.config['LIQPAY_MERCHANT'], current_app.config['LIQPAY_SECRET'])
