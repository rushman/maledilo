#!coding=utf-8
from flask_wtf import Form
from wtforms import TextField, DecimalField, TextAreaField, FloatField
from wtforms.widgets import HiddenInput
from wtforms.validators import DataRequired


class AddDeloForm(Form):
    title = TextField(u'Название дела', validators=[DataRequired()])
    teaser = TextField(u'Краткое описание', validators=[DataRequired()])
    description = TextAreaField(u'Описание', validators=[DataRequired()])
    goal = DecimalField(u'Необходимая сумма', places=0, validators=[DataRequired()])

    position_lat = FloatField(validators=[DataRequired()], widget=HiddenInput())
    position_lng = FloatField(validators=[DataRequired()], widget=HiddenInput())
