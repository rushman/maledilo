import decimal
import datetime
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy.ext.hybrid import hybrid_property

from .postgis import *
from .app import app

db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True)
    first_name = db.Column(db.Unicode(120))
    last_name = db.Column(db.Unicode(120))

    num_backed = db.Column(db.Integer, default=0)

    def __init__(self, email):
        self.email = email

    @property
    def full_name(self):
        return '{0} {1}'.format(self.first_name, self.last_name)

    def __repr__(self):
        return '<User %r>' % self.email


class Delo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    position = db.Column(Geography)
    goal = db.Column(db.Numeric(10, 2))
    pledged = db.Column(db.Numeric(10, 2), default=0)
    num_backers = db.Column(db.Integer, default=0)
    title = db.Column(db.UnicodeText(), nullable=False)
    teaser = db.Column(db.UnicodeText())
    description_id = db.Column(db.Integer)
    approved_at = db.Column(db.DateTime)
    ends_at = db.Column(db.DateTime)

    curator_id = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)

    curator = db.relation(User)
    description = db.relation("DeloDescription", uselist=False)

    @hybrid_property
    def is_approved(self):
        return self.approved_at is not None

    @is_approved.expression
    def is_approved(cls):
        return cls.approved_at != None

    @property
    def progress(self):
        return (100 * self.pledged / self.goal).quantize(decimal.Decimal(1))
    

class DeloDescription(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    delo_id = db.Column(db.Integer, db.ForeignKey(Delo.id))
    time = db.Column(db.DateTime())
    html = db.Column(db.UnicodeText)


class Pledge(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    delo_id = db.Column(db.Integer, db.ForeignKey(Delo.id))
    amount = db.Column(db.Numeric(10, 2))

    last_status = db.Column(db.String(100))
    last_code = db.Column(db.String(100))
    last_description = db.Column(db.UnicodeText)

    created_at = db.Column(db.DateTime(), default=datetime.datetime.now())
    completed_at = db.Column(db.DateTime())

    delo = db.relation(Delo)
    user = db.relation(User)

    @hybrid_property
    def is_completed(self):
        return self.completed_at is not None

    @is_completed.expression
    def is_approved(cls):
        return cls.completed_at != None
