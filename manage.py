#!/usr/bin/env python
#!coding=utf-8
from flask.ext.script import Manager

from delo.app import app

manager = Manager(app)


@manager.command
def hello():
    print "hello"


@manager.command
def initdb():
    """Create database tables"""
    from delo.db import *
    db.create_all()

    u = User.query.get(1)
    if not u:
        u = User(email='sergey.kirillov@gmail.com')#, first_name=u'Sergey', last_name=u'Kirillov')
        db.session.add(u)
        db.session.commit()

    # d = Delo.query.get(1)
    # if not d:
    #     d = Delo(curator=u, 
    #              title=u'Grokhnut Yanika', 
    #              teaser=u'грохнуть совсем',
    #              goal=80000.50, 
    #              position="POINT(50.614972232222 30.474305565556)", 
    #              description=DeloDescription(html="Prosto grokhnut'"),
    #              approved_at=datetime.datetime.now())
    #     db.session.add(d)

        # d = Delo(curator=u, 
        #          title=u'Grokhnut Azirova', 
        #          teaser=u'грохнуть подлеца',
        #          goal=80000.50, 
        #          position="POINT(50.614972232222 30.474305565556)", 
        #          description=DeloDescription(html="Prosto grokhnut'"),
        #          approved_at=datetime.datetime.now())
        # db.session.add(d)
    #     db.session.commit()
    # else:
    #     print repr(d.position)

if __name__ == "__main__":
    manager.run()
